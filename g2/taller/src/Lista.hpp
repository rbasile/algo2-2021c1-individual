#include "Lista.h"

Lista::Lista() : primero(nullptr), ultimo(nullptr), longitud_(0) {
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    while (longitud() > 0)
    	eliminar(0);
}

Lista& Lista::operator=(const Lista& aCopiar) {
    while (longitud() > 0)
    	eliminar(0);
    	
	for (int i = 0; i < aCopiar.longitud(); ++i)
		agregarAtras(aCopiar.iesimo(i));
	
    return *this;
}

void Lista::agregarAdelante(const Nat& elem) {
	Nodo* nodo = new Nodo{nullptr, elem, nullptr};
	
	if (longitud() == 0) {
		primero = nodo;
		ultimo = nodo;
	} else {
		nodo->siguiente = primero;
		primero->anterior = nodo;
		primero = nodo;
	}
	
	longitud_++;
}

void Lista::agregarAtras(const Nat& elem) {
	Nodo* nodo = new Nodo{nullptr, elem, nullptr};
	
	if (longitud() == 0) {
		primero = nodo;
		ultimo = nodo;
	} else {
		nodo->anterior = ultimo;
		ultimo->siguiente = nodo;
		ultimo = nodo;
	}
	
	longitud_++;
}

void Lista::eliminar(Nat i) {
	Nodo *nodo = primero;
	
    for (int j = 0; j < i; ++j)
    	nodo = nodo->siguiente;
    
    if (nodo->anterior)
		nodo->anterior->siguiente = nodo->siguiente;
	else
		primero = nodo->siguiente;
		
    if (nodo->siguiente)
		nodo->siguiente->anterior = nodo->anterior;
	else
		ultimo = nodo->anterior;
		
	delete nodo;
	
	longitud_--;
}

int Lista::longitud() const {
    return longitud_;
}

const int& Lista::iesimo(Nat i) const {
	Nodo *nodo = primero;
	
    for (int j = 0; j < i; ++j)
    	nodo = nodo->siguiente;
    	
	return nodo->elem;
}

int& Lista::iesimo(Nat i) {
	Nodo *nodo = primero;
	
    for (int j = 0; j < i; ++j)
    	nodo = nodo->siguiente;
    	
	return nodo->elem;
}

void Lista::mostrar(ostream& o) {
    // Completar
}
