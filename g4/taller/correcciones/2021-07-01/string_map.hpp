#include <iostream>

template <typename T>
string_map<T>::string_map() {
	raiz = new Nodo("");
	_size = 0;
	claves = vector<string>{""};
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
	// Borrar todas las claves del trie
	for (int i = 0; i < claves.size(); ++i) {
		if (count(claves[i]))
			erase(claves[i]);
	}
	claves.clear();
	
	// Agregar cada clave del otro trie a este
	raiz = new Nodo("");
	
	for (int i = 0; i < d.claves.size(); ++i) {
		if (d.count(d.claves[i])) { // Evitar claves fantasma
			insert(pair<string, T>(d.claves[i], d.at(d.claves[i])));
			claves.push_back(d.claves[i]);
		}
	}
	
	_size = d._size;
	
	return *this;
}

template <typename T>
string_map<T>::~string_map() {
	for (int i = 0; i < claves.size(); ++i) {
		if (count(claves[i])) // Evitar claves fantasma
			erase(claves[i]);
	}
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    return at(clave);
}

template <typename T>
void string_map<T>::insert(const pair<string, T>& value_type) {
	Nodo* iterador = raiz;
	string clave = value_type.first;
	T definicion = value_type.second;
	
	// Itera creando un nodo cuando la letra no tenga esté definida
	for (int i = 0; i < clave.length(); ++i) {
		if (!iterador->siguientes[(int)clave[i]])
			iterador->siguientes[(int)clave[i]] = new Nodo(clave);
		
		iterador = iterador->siguientes[(int)clave[i]];
	}
	
	// Solo aumentar tamaño si no es una sobreescritura
	if (iterador->definicion)
		delete iterador->definicion;
	else
		_size++;
		
	// Definir o sobreescribir clave
	iterador->definicion = new T{definicion};
	claves.push_back(clave); // Está bien que haya claves fantasma acá
}

template <typename T>
int string_map<T>::count(const string& clave) const{
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length() && iterador; ++i)
		iterador = iterador->siguientes[(int)clave[i]];
	
	return iterador && iterador->clave == clave && iterador->definicion;
}

// Pre: La clave está definida
template <typename T>
const T& string_map<T>::at(const string& clave) const {
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length(); ++i)
		iterador = iterador->siguientes[(int)clave[i]];
	
	return *iterador->definicion;
}

// Pre: La clave está definida
template <typename T>
T& string_map<T>::at(const string& clave) {
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length(); ++i)
		iterador = iterador->siguientes[(int)clave[i]];
	
	return *iterador->definicion;
}

// Pre: La clave está definida
template <typename T>
void string_map<T>::erase(const string& clave) {
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length(); ++i)
		iterador = iterador->siguientes[(int)clave[i]];
		
	// Borrar definición
	if (iterador->definicion) {
		delete iterador->definicion;
		iterador->definicion = nullptr;
		_size--;
	}
	
	// Determinar si nodo a borrar es hoja
	bool hoja = true;
	for (int i = 0; i < iterador->siguientes.size() && hoja; ++i)
		hoja = !iterador->siguientes[i];
	
	// Borrar el nodo en sí si es hoja
	if (hoja) {
		delete iterador;
		iterador = nullptr;
		
		// Al borrar este nodo, sus padres pueden resultar redundantes
		// Recorrer hacia arriba y borrar hasta toparse con un nodo definido
		for (int i = clave.length() - 1; i > 0; --i) {
			string pre = clave.substr(0, i);
			if (!count(pre))
				erase(pre);
			else
				break; // Feo :(
		}
	}
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
	return !_size;
}

template <typename T>
string_map<T>::Nodo::Nodo(string c) {
	siguientes = vector<Nodo*>(MAX_CHARS, nullptr);
	clave = c;
	definicion = nullptr;
}
