template <typename T>
string_map<T>::string_map() {
	raiz = new Nodo();
	_size = 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
	delete raiz;
	raiz = new Nodo(*d.raiz);
	
	_size = d._size;
}

template <typename T>
string_map<T>::~string_map(){
    delete raiz;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    return at(clave);
}

template <typename T>
void string_map<T>::insert(const pair<string, T>& value_type) {
	Nodo* iterador = raiz;
	string clave = value_type.first;
	
	// Itera creando un nodo cuando la letra no tenga esté definida
	for (int i = 0; i < clave.length(); ++i) {
		if (!iterador->siguientes[(int)clave[i]])
			iterador->siguientes[(int)clave[i]] = new Nodo();
		
		iterador = iterador->siguientes[(int)clave[i]];
	}
	
	// Sobreescribe si ya está definida, inserta si no lo está
	if (iterador->definicion)
		delete iterador->definicion;
	else
		_size++;
		
	iterador->definicion = new T{value_type.second};
}

template <typename T>
int string_map<T>::count(const string& clave) const{
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length() && iterador; ++i)
		iterador = iterador->siguientes[(int)clave[i]];
	
	return (int)(iterador && iterador->definicion);
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
	return at(clave);
}

// Pre: La clave está definida
template <typename T>
T& string_map<T>::at(const string& clave) {
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length(); ++i)
		iterador = iterador->siguientes[(int)clave[i]];
	
	return *iterador->definicion;
}

// Pre: La clave está definida
template <typename T>
void string_map<T>::erase(const string& clave) {
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length(); ++i)
		iterador = iterador->siguientes[(int)clave[i]];
		
	delete iterador->definicion;
	iterador->definicion = nullptr;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
	return !_size;
}

template <typename T>
string_map<T>::Nodo::Nodo() {
	siguientes = vector<Nodo*>(MAX_CHARS, nullptr);
	definicion = nullptr;
}

// Copia
template <typename T>
string_map<T>::Nodo::Nodo(const Nodo& otro) {
	siguientes = vector<Nodo*>(MAX_CHARS, nullptr);
	definicion = nullptr;
	
	// Copia siguientes no nulos
	for (int i = 0; i < otro.siguientes.size(); ++i) {
		if (otro.siguientes[i])
			siguientes[i] = new Nodo(*otro.siguientes[i]);
	}
	
	// Copia definición por valor si es que otro tiene
	if (otro.definicion)
		definicion = otro.definicion; 
}

template <typename T>
string_map<T>::Nodo::~Nodo() {
	for (int i = 0; i < siguientes.size(); ++i)
		delete siguientes[i];
	
	delete definicion;
	definicion = nullptr;
}
