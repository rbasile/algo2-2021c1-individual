#include <iostream>

template <typename T>
string_map<T>::string_map() {
	raiz = new Nodo("");
	_size = 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
	// Borrar todas las claves del trie
	set<string> copia = claves;
	for (string clave : copia)
		erase(clave);
		
	delete raiz;
	
	claves.clear();
	
	// Agregar cada clave del otro trie a este
	raiz = new Nodo("");
	
	for (string clave : d.claves) {
		insert(pair<string, T>(clave, d.at(clave)));
		claves.insert(clave);
	}
	
	_size = d._size;
	
	return *this;
}

template <typename T>
string_map<T>::~string_map() {
	set<string> copia = claves;
	for (string clave : copia)
		erase(clave);
		
	delete raiz;
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    return at(clave);
}

template <typename T>
void string_map<T>::insert(const pair<string, T>& value_type) {
	Nodo* iterador = raiz;
	string clave = value_type.first;
	T definicion = value_type.second;
	
	// Itera creando un nodo cuando la letra no tenga esté definida
	for (int i = 0; i < clave.length(); ++i) {
		if (!iterador->siguientes[(int)clave[i]])
			iterador->siguientes[(int)clave[i]] = new Nodo(clave.substr(0, i + 1));
		
		iterador = iterador->siguientes[(int)clave[i]];
	}
	
	// Sobreescritura
	if (iterador->definicion) {
		delete iterador->definicion;
		claves.erase(clave);
	} else
		_size++;
	
	iterador->definicion = new T{definicion};
	claves.insert(clave);
}

template <typename T>
int string_map<T>::count(const string& clave) const{
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length() && iterador; ++i)
		iterador = iterador->siguientes[(int)clave[i]];
	
	return iterador && iterador->clave == clave && iterador->definicion;
}

// Pre: La clave está definida
template <typename T>
const T& string_map<T>::at(const string& clave) const {
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length(); ++i)
		iterador = iterador->siguientes[(int)clave[i]];
	
	return *iterador->definicion;
}

// Pre: La clave está definida
template <typename T>
T& string_map<T>::at(const string& clave) {
	Nodo* iterador = raiz;
	
	for (int i = 0; i < clave.length(); ++i)
		iterador = iterador->siguientes[(int)clave[i]];
	
	return *iterador->definicion;
}

// Pre: La clave está definida
template <typename T>
void string_map<T>::erase(const string& clave) {
	Nodo* iterador = raiz;
	Nodo* padre = nullptr;
		
	// Buscar nodo a borrar
	for (int i = 0; i < clave.length(); ++i) {
		padre = iterador;
		iterador = iterador->siguientes[(int)clave[i]];
	}
	
	// Borrar definición
	if (iterador->definicion) {
		delete iterador->definicion;
		iterador->definicion = nullptr;
		
		claves.erase(clave);
		
		_size--;
	}
	
	// Determinar si nodo a borrar es hoja
	bool tiene_siguientes = false;
	for (int i = 0; i < iterador->siguientes.size() && !tiene_siguientes; ++i)
		tiene_siguientes = iterador->siguientes[i];
	
	// Borrar el nodo en sí si es hoja
	if (!tiene_siguientes && padre && !padre->definicion) {
		padre->siguientes[(int)clave.back()] = nullptr;
		delete iterador;
		
		// Al borrar este nodo, sus padres pueden resultar redundantes
		// Recorrer hacia arriba y borrar hasta toparse con un nodo definido
		erase(padre->clave);	
	}
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
	return !_size;
}

template <typename T>
string_map<T>::Nodo::Nodo(string c) {
	siguientes = vector<Nodo*>(MAX_CHARS, nullptr);
	clave = c;
	definicion = nullptr;
}
