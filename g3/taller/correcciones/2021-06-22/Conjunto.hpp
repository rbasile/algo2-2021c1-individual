template <class T>
Conjunto<T>::Conjunto() : _raiz(nullptr), _cardinal(0) {
}

template <class T>
Conjunto<T>::~Conjunto() {
	remover(_raiz->valor);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
	return obtenerNodo(clave);
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
	// Caso 1: Ya hay raíz
	if (_raiz) {
		Nodo* iterador = _raiz;
		Nodo* padre = _raiz;
		
		// Buscar padre iterando hasta que iterador sea hoja
		while (iterador && clave != iterador->valor) {
			padre = iterador;
			
			if (clave < iterador->valor) 
				iterador = iterador->izq;
			else 
				iterador = iterador->der;
		}
		
		// Insertar nodo si iterador es hoja y el padre no es esta clave
		if (!iterador && clave != padre->valor) {
			if (clave < padre->valor) 
				padre->izq = new Nodo(clave, padre);
			else 
				padre->der = new Nodo(clave, padre);
				
			_cardinal++;
		}
		
	// Caso 2: No hay raíz
	} else {
		_raiz = new Nodo(clave);
		_cardinal++;
	}
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
	Nodo* nodo = obtenerNodo(clave);
	
	if (nodo) {
		if (nodo->der && nodo->izq) {
			// Buscamos al predecesor inmediato, el nodo más a la derecha del
			// subárbol izquierdo del nodo que estamos borrando
			Nodo* pi = nodo->izq;
			while (pi->der)
				pi = pi->der;
			
			// Sacamos al predecesor inmediato del arbol y luego copiamos su
			// valor al nodo que estamos borrando
			T copia = pi->valor;
			remover(pi->valor);
			nodo->valor = copia;
		} else {
			if (nodo->padre && nodo->der) {
				nodo->der->padre = nodo->padre;
				
				if (nodo == nodo->padre->der)
					nodo->padre->der = nodo->der;
				else
					nodo->padre->izq = nodo->der;
			} else if (nodo->padre && nodo->izq) {
				nodo->izq->padre = nodo->padre;
				
				if (nodo == nodo->padre->izq)
					nodo->padre->izq = nodo->izq;
				else
					nodo->padre->der = nodo->izq;
			} else if (nodo == _raiz)
				_raiz = nullptr;
				
			delete nodo;
			_cardinal--;
		}
	}
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
	Nodo* si = obtenerNodo(clave);
	
	// Caso 1: Cuando el nodo tiene der, agarramos el más a la izquierda de der
	if (si->der) {
		si = si->der;
		
		while (si->izq)
			si = si->izq;
			
	// Caso 2: Si no tiene nada a la derecha, debe ser su padre el siguiente
	} else
		si = si->padre;
	
	return si->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
	Nodo* iterador = _raiz;
	
	while (iterador->izq)
		iterador = iterador->izq;
		
	return iterador->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
	Nodo* iterador = _raiz;
	
	while (iterador->der)
		iterador = iterador->der;
		
	return iterador->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

template <class T>
Conjunto<T>::Nodo::Nodo(const T& v, Nodo* padre) 
		: valor(v), padre(padre), izq(nullptr), der(nullptr) {
}

template <class T>
typename Conjunto<T>::Nodo* Conjunto<T>::obtenerNodo(const T& clave) const {
	Nodo* iterador = _raiz;
		
	while (iterador && iterador->valor != clave) {
		if (clave < iterador->valor) 
			iterador = iterador->izq;
		else if (clave > iterador->valor) 
			iterador = iterador->der;
		else
			iterador = nullptr;
	}
	
	return iterador;
}
