#include <utility>

using namespace std;

// Ejercicio 15

using Pos = pair<int, int>;

struct Pocion {
	uint movimientos;
	uint fin;
};

const char ARRIBA = '^';
const char ABAJO = 'v'; 
const char DERECHA = '<';
const char IZQUIERDA = '>';

class Juego {
	public:
	Juego(uint casilleros, Pos pos_inicial);
	
	Pos posicion_jugador();
	uint turno_actual();
	void mover_jugador(char dir);
	void ingerir_pocion(uint movimientos, uint turnos);
	void pasar_turno();
	
	private:
	uint turno = 0;
	uint tamaño_mapa;
	Pos pos_jugador;
	vector<Pocion> pociones;
	uint movs_restantes = 1;
};

Juego::Juego(uint casilleros, Pos pos_inicial) 
		: tamaño_mapa(casilleros), pos_jugador(pos_inicial) {}
		
Pos Juego::posicion_jugador() {
	return pos_jugador;
}
		
uint Juego::turno_actual() {
	return turno;
}

void Juego::mover_jugador(char dir) {
	Pos movimiento;
	switch (dir) {
		case ARRIBA:
			movimiento.first = -1;
			break;
		case ABAJO:
			movimiento.first = 1;
			break;
		case DERECHA:
			movimiento.second = 1;
			break;
		case IZQUIERDA:
			movimiento.second = -1;
			break;
	}
	
	if (0 <= movimiento.first < tamaño_mapa 
			&& 0 <= movimiento.second < tamaño_mapa) {
		pos_jugador.first += movimiento.first;
		pos_jugador.second += movimiento.second;
		
		movs_restantes--;
		if (movs_restantes <= 1)
			pasar_turno();
	}
}

void Juego::ingerir_pocion(uint movimientos, uint turnos) {
	pociones.push_back(Pocion{movimientos, turnos + turno_actual()});
	
	if (turnos > 0)
		movs_restantes += movimientos;
}

void Juego::pasar_turno() {
	turno++;
	movs_restantes = 1;
	
	// Aplicar pociones
	for (int i = 0; i < pociones.size(); ++i) {
		if (pociones[i].fin > turno_actual())
			movs_restantes += pociones[i].movimientos;
	}
}
