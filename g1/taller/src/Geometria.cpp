#include <iostream>

using namespace std;

using uint = unsigned int;


// Ejercicio 1

class Rectangulo {
    public:
        Rectangulo(uint alto, uint ancho);
        uint alto();
        uint ancho();
        float area();

    private:
        int alto_;
        int ancho_;

};

Rectangulo::Rectangulo(uint alto, uint ancho) : alto_(alto), ancho_(ancho) {};

uint Rectangulo::alto() {
	return alto_;
}

uint Rectangulo::ancho() {
	return ancho_;
}

float Rectangulo::area() {
	return alto() * ancho();
}


// Ejercicio 2

const float PI = 3.14f;
	
class Elipse {
	public:
	Elipse(uint a, uint b);
	
	uint r_a();
	uint r_b();
	float area();
	
	private:
	uint a;
	uint b;
};

Elipse::Elipse(uint a, uint b) : a(a), b(b) {}

uint Elipse::r_a() {
	return a;
}

uint Elipse::r_b() {
	return b;
}

float Elipse::area() {
	return PI * r_a() * r_b();
}


// Ejercicio 3

class Cuadrado {
    public:
        Cuadrado(uint lado);
        uint lado();
        float area();

    private:
        Rectangulo r_;
};

Cuadrado::Cuadrado(uint lado): r_(lado, lado) {}

uint Cuadrado::lado() {
    return r_.alto();
}

float Cuadrado::area() {
	return r_.area();
}

// Ejercicio 4

class Circulo {
	public:
	Circulo(uint radio);
	
	uint radio();
	float area();
	
	private:
	Elipse elipse;
};

Circulo::Circulo(uint radio) : elipse(radio, radio) {}

uint Circulo::radio() {
	return elipse.r_a();
}

float Circulo::area() {
	return elipse.area();
}


// Ejercicio 5

ostream& operator<<(ostream& os, Rectangulo r) {
    os << "Rect(" << r.alto() << ", " << r.ancho() << ")";
    return os;
}

ostream& operator<<(ostream& os, Elipse elipse) {
    os << "Elipse(" << elipse.r_a() << ", " << elipse.r_b() << ")";
    return os;
}


// Ejercicio 6

ostream& operator<<(ostream& os, Cuadrado cuadrado) {
    os << "Cuad(" << cuadrado.lado() << ")";
    return os;
}

ostream& operator<<(ostream& os, Circulo circulo) {
    os << "Circ(" << circulo.radio() << ")";
    return os;
}
