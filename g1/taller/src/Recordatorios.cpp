#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
	public:
	Fecha(int mes, int dia);
	
	#if EJ >= 9 // Para ejercicio 9
	bool operator==(Fecha o);
	#endif
	#if EJ >= 13 // Para ejercicio 14
	bool operator<(Fecha otra);
	#endif
	
	int mes();
	int dia();
	void incrementar_dia();

	private:
	int mes_;
	int dia_;
};

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {}

int Fecha::mes() {
	return mes_;
}

int Fecha::dia() {
	return dia_;
}

void Fecha::incrementar_dia() {
	if (dia() < dias_en_mes(mes()))
		dia_++;
	else if (mes() < 12) {
		dia_ = 1;
		mes_++;
	} else {
		dia_ = 1;
		mes_ = 1;
	}
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_mes = this->mes() == o.mes();
    bool igual_dia = this->dia() == o.dia();
    return igual_mes && igual_dia;
}
#endif

#if EJ >= 13
bool Fecha::operator<(Fecha otra) {
    bool menor_mes = this->mes() < otra.mes();
    bool igual_mes = this->mes() == otra.mes();
    bool menor_dia = this->dia() < otra.dia();
    return menor_mes || (igual_mes && menor_dia);
}
#endif

ostream& operator<<(ostream& os, Fecha fecha) {
    os << fecha.dia() << "/" << fecha.mes();
    return os;
}

// Ejercicio 11, 12

class Horario {
	public:
	Horario(int hora, int min);
	
	bool operator==(Horario otro);
	bool operator<(Horario otro);
	
	int hora();
	int min();
	
	private:
	int hora_;
	int min_;
};

Horario::Horario(int hora, int min) : hora_(hora), min_(min) {}

int Horario::hora() {
	return hora_;
}

int Horario::min() {
	return min_;
}

bool Horario::operator==(Horario otro) {
    bool igual_hora = this->hora() == otro.hora();
    bool igual_min = this->min() == otro.min();
    return igual_hora && igual_min;
}

bool Horario::operator<(Horario otro) {
    bool menor_hora = this->hora() < otro.hora();
    bool misma_hora = this->hora() == otro.hora();
    bool menor_min = this->min() < otro.min();
    return menor_hora || (misma_hora && menor_min);
}

ostream& operator<<(ostream& os, Horario horario) {
    os << horario.hora() << ":" << horario.min();
    return os;
}

// Ejercicio 13

class Recordatorio {
	public:
	Recordatorio(Fecha fecha, Horario horario, string mensaje);
	
	#if EJ >= 13 // Para poder usar la función sort() en el ejercicio 14
	bool operator<(Recordatorio otro);
	#endif
	
	Fecha fecha();
	Horario horario();
	string mensaje();
	
	private:
	Fecha fecha_;
	Horario horario_;
	string mensaje_;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, string mensaje)
		: fecha_(fecha), horario_(horario), mensaje_(mensaje) {}

#if EJ >= 13
bool Recordatorio::operator<(Recordatorio otro) {
    bool menor_fecha = this->fecha() < otro.fecha();
    bool misma_fecha = this->fecha() == otro.fecha();
    bool menor_horario = this->horario() < otro.horario();
    return menor_fecha || (misma_fecha && menor_horario);
}
#endif

Fecha Recordatorio::fecha() {
	return fecha_;
}

Horario Recordatorio::horario() {
	return horario_;
}

string Recordatorio::mensaje() {
	return mensaje_;
}

ostream& operator<<(ostream& os, Recordatorio rec) {
    os << rec.mensaje() << " @ " << rec.fecha() << " " << rec.horario();
    return os;
}

// Ejercicio 14

class Agenda {
	public:
	Agenda(Fecha fecha_inicial);
	
	void agregar_recordatorio(Recordatorio rec);
	void incrementar_dia();
	list<Recordatorio> recordatorios_de_hoy();
	Fecha hoy();
	
	private:
	list<Recordatorio> recordatorios;
	Fecha hoy_;
};

Agenda::Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial) {}

void Agenda::agregar_recordatorio(Recordatorio rec) {
	recordatorios.push_back(rec);
	recordatorios.sort();
}

void Agenda::incrementar_dia() {
	hoy_.incrementar_dia();
}

#if EJ >= 13
list<Recordatorio> Agenda::recordatorios_de_hoy() {
	list<Recordatorio> de_hoy(recordatorios);
	
	for (int i = 0; i < recordatorios.size(); ++i) {
		if (!(de_hoy.front().fecha() == hoy()))
			de_hoy.pop_front();
	}
	
	return de_hoy;
}
#endif

Fecha Agenda::hoy() {
	return hoy_;
}

#if EJ >= 13
ostream& operator<<(ostream& os, Agenda agenda) {
	os << agenda.hoy() << endl << "=====" << endl;
	
	list<Recordatorio> recs(agenda.recordatorios_de_hoy());
	for (int i = 0; i < agenda.recordatorios_de_hoy().size(); ++i) {
		os << recs.front() << endl;
		recs.pop_front();
	}
    
    return os;
}
#endif
